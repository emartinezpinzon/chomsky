Convertidor de gramáticas libres de contexto a forma normal de Chomsky
======================================================================

Ejecutando el algoritmo de este script puede convertir gramáticas libres de contexto a la forma normal de Chomsky.

El proceso se enfoca en 3 partes:

1. Elimina reglas vacías
2. Elimina reglas cortas
3. Elimina reglas largas

Esto para dejar sólo reglas que se ajustan a la forma normal de Chomsky.


Cómo ejecutar el script
-----------------------

Este script está escrito en Python y debe correr en la versión 2.7 o similares.


Para ejecutarlo debe tener esta versión en su sistema e invocar el archivo como script desde una consola.

	$ python Chomsky.py

![Ejecución del programa](https://i.imgur.com/wxrUZWF.png)