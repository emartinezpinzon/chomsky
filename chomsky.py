#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
 @author: Emanuel Martínez - 1151245
 Teoría de la computación
 UFPS - 2019
"""

#Importaciones
from string import letters

import re
import copy

class Chomsky:
    # Remueve reglas vacías (Declaradas como X e o similares)
    def reg_Vacias(self,reglas,voc):
        e_list = []

        # Encuentra las variables no terminales y las agrega al diccionario
        new_dict = copy.deepcopy(reglas)
        for key in new_dict:
            values = new_dict[key]
            for i in range(len(values)):
                if values[i] == 'e' and key not in e_list:
                    e_list.append(key)
                    reglas[key].remove(values[i])

            # Si no contiene ningún valor limpia el diccionario
            if len(reglas[key]) == 0:
                if key not in reglas:
                    voc.remove(key)
                reglas.pop(key, None)

        # Elimina las reglas vacías
        new_dict = copy.deepcopy(reglas)
        for key in new_dict:
            values = new_dict[key]
            for i in range(len(values)):
                if len(values[i]) == 2:
                    if values[i][0] in e_list and key!=values[i][1]:
                        reglas.setdefault(key, []).append(values[i][1])
                    
                    if values[i][1] in e_list and key!=values[i][0]:
                        if values[i][0]!=values[i][1]:
                            reglas.setdefault(key, []).append(values[i][0])

        return reglas,voc
     

    # Remueve las reglas cortas (Del tipo A->B)
    def reg_Cortas(self,reglas,voc):

        D = dict(zip(voc, voc))

        for key in D:
            D[key] = list(D[key])

        for letter in voc:
            for key in reglas:
                if key in D[letter]:
                    values = reglas[key]
                    for i in range(len(values)):
                        if len(values[i]) == 1 and values[i] not in D[letter]:
                            D.setdefault(letter, []).append(values[i])

        reglas,D = self.cortas_Aux(reglas,D)
        return reglas,D


    def cortas_Aux(self,reglas,D):

        # Remueve las reglas cortas (Cuando la longitud en el lado derecho es igual a 1)
        new_dict = copy.deepcopy(reglas)
        for key in new_dict:
            values = new_dict[key]
            for i in range(len(values)):
                if len(values[i]) == 1:
                    reglas[key].remove(values[i])
            if len(reglas[key]) == 0: reglas.pop(key, None)

        for key in reglas:
            values = reglas[key]
            for i in range(len(values)):
                for j in D[values[i][0]]:
                    for k in D[values[i][1]]:
                        if j+k not in values:
                            reglas.setdefault(key, []).append(j + k)

        return reglas,D


    # Remueve reglas largas (Reglas que cuentan con más de dos estados declarados)
    def reg_Largas(self,reglas,let,voc):
        new_dict = copy.deepcopy(reglas)
        for key in new_dict:
            values = new_dict[key]
            for i in range(len(values)):
                if len(values[i]) > 2:
                    for j in range(0, len(values[i]) - 2):
                        if j==0:
                            reglas[key][i] = reglas[key][i][0] + let[0]
                        else:
                            reglas.setdefault(new_key, []).append(values[i][j] + let[0])
                        voc.append(let[0])

                        new_key = copy.deepcopy(let[0])

                        let.remove(let[0])

                    reglas.setdefault(new_key, []).append(values[i][-2:])

        return reglas,let,voc


    # Inserta reglas finales al diccionario de reglas
    def finales(self,reglas,D,S):

        for let in D[S]:
            if not reglas[S] and not reglas[let]:
                for v in reglas[let]:
                    if v not in reglas[S]:
                        reglas.setdefault(S, []).append(v)
        return reglas

    # Imprimir reglas
    def imprimir(self,reglas):
        for key in reglas:
            values = reglas[key]
            for i in range(len(values)):
                print key + '->' + values[i]
        return 1


def main():
    ch = Chomsky()

    reglas = {}
    voc = []
    let = list(letters[26:]) + list(letters[:25])
    let.remove('e')

    # Establece el número de reglas de gramática
    while True:
        numReglas = raw_input('Ingrese el número de reglas: ')
        try:
            NR = int(numReglas)
            if NR <= 2: print 'Deben existir mínimo 2 reglas'
            else: break
        except ValueError:
            print "Sólo se aceptan enteros"

    # Establece la variable inicial
    while True:
        S = raw_input('Ingrese la variable inicial: ')
        if not re.match("[a-zA-Z]*$", S): print 'La variable inicial debe ser una letra, ya sea a-z ó A-Z.'
        else:break

    print '+-----------------------------------------------------------------+'
    print '|Ingrese las reglas para la forma A B (Separado por espacios)     |'
    print '|                                                                 |'
    print '|Para A->B o A BCD, si hay más de un estado en la parte derecha   |'
    print '|ingreselos sin espacio entre ellos.                              |'
    print '+-----------------------------------------------------------------+'

    for i in range(NR):
        fr, to = map(str,raw_input('Regla #' + str(i + 1) + ' ').split())

        for l in fr:
            if l!='e' and l not in voc: voc.append(l)
            if l in let: let.remove(l)
        for l in to:
            if l!='e' and l not in voc: voc.append(l)
            if l in let: let.remove(l)

        reglas.setdefault(fr, []).append(to)

    # Solicita las reglas luego de eliminar reglas vacías y las imprime
    print '\nListado resultante luego de eliminar reglas vacías'
    reglas,voc = ch.reg_Vacias(reglas,voc)
    ch.imprimir(reglas)

    # Solicita las reglas luego de eliminar reglas cortas y las imprime
    print '\nListado resultante luego de eliminar reglas cortas'
    reglas,D = ch.reg_Cortas(reglas,voc)
    ch.imprimir(reglas)

    # Solicita las reglas luego de eliminar las reglas largas y las imprime
    print '\nListado resultante luego de eliminar reglas largas'
    reglas,let,voc = ch.reg_Largas(reglas,let,voc)
    ch.imprimir(reglas)

    # Solicita e imprime las reglas finales
    print '\nReglas finales'
    reglas = ch.finales(reglas,D,S)
    ch.imprimir(reglas)

if __name__ == '__main__':
    main()
